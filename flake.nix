{
  description = "EDeA Server";

  # Nixpkgs / NixOS version to use.
  inputs.nixpkgs.url = "nixpkgs/nixos-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem 
      (system:
        let 
          pkgs = import nixpkgs { inherit system; };
          edea-frontend = pkgs.mkYarnPackage rec {
            name = "edea-frontend";
            version = (pkgs.lib.importJSON (src + "/package.json")).version;
            src = ./frontend;
            postBuild = ''
              cd deps/edea-frontend
              rm -rf node_modules
              ln -s "../../node_modules" .
              bash ./build-fe.sh
              mkdir -p $out
              mv ../static $out/
              cd ../..
            '';
          };
        in
        {
          devShell = pkgs.mkShell {
            buildInputs = [
              pkgs.go
              pkgs.gotools
              pkgs.golangci-lint
              pkgs.gopls
              pkgs.go-outline
              pkgs.gopkgs
            ];
          };

          packages.default = pkgs.buildGo119Module {
              pname = "edea-server";
              name = "edea-server";

              src = ./.;

              subPackages = [ "cmd/edea-server" ];

              # vendorSha256 = pkgs.lib.fakeSha256;

              vendorSha256 = "sha256-b+IpZEULz9jGgoLyHsW0xQk2Oec3aDO/u/jl4qimk5U=";

              preBuild = ''
                cp -r ${edea-frontend}/static ./.
              '';

              postInstall =
                ''
                  mkdir -p $out/bin $out/share/edea-server/

                  cp -r ${edea-frontend}/static $out/share/edea-server/static

                  cp -r ${./frontend/template} $out/share/edea-server/template
                '';
              
              passthru = { inherit edea-frontend; };
          };
      }
      );
}
